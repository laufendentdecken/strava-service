package at.running.platform.strava.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonParseException;

import at.running.platform.common.Command;
import at.running.platform.common.model.Help;
import at.running.platform.strava.token.StravaConnector;
import at.running.platform.strava.utils.JsonUtils;
import at.running.platform.strava.utils.ParamExtractor;
import javastrava.api.API;
import javastrava.model.StravaActivity;
import lombok.Setter;

@CrossOrigin(origins = { "http://localhost:8080" })
@Controller
public class StravaController extends Command {

	private final static String CONTROLLER_NAME = "strava";

	@Autowired
	private JsonUtils jsonUtils;

	@Autowired
	@Setter
	private StravaConnector stravaConnector;

	@GetMapping("/" + CONTROLLER_NAME + "/list/{count}")
	@ResponseBody
	public String list(@PathVariable("count") Integer count,
			@RequestParam(value = "arg", required = false) String arg) {
		StringBuffer buffer = new StringBuffer();
		API api = stravaConnector.getAPI();

		StravaActivity[] activities = api.listAuthenticatedAthleteActivities(null, null, 1, count);
		buffer.append("[");
		if (StringUtils.isNotEmpty(arg)) {
			Arrays.stream(activities).forEach(a -> {
				buffer.append("{");
				Arrays.stream(arg.split(",")).forEach(v -> {
					buffer.append(ParamExtractor.extract(a, v) + ",");
				});
				buffer.setLength(buffer.length() - 1);
				buffer.append("},");
			});
		} else {
			Arrays.stream(activities)
					.forEach(a -> buffer.append("{\"id\":" + a.getId() + ",\"name\":\"" + a.getName() + "\"},"));
		}
		buffer.setLength(buffer.length() - 1);
		buffer.append("]");

		return buffer.toString();
	}

	@GetMapping("/" + CONTROLLER_NAME + "/get/{id}")
	@ResponseBody
	public String get(@PathVariable("id") Long id) {
		return getActivity(id);
	}

	@GetMapping("/" + CONTROLLER_NAME + "/var")
	@ResponseBody
	public String var() {
		StringBuffer buffer = new StringBuffer();
		Arrays.stream(StravaActivity.class.getDeclaredFields()).forEach(f -> {
			buffer.append(f.getName() + " | ");
		});

		return buffer.toString();
	}

	@PostMapping("/" + CONTROLLER_NAME + "/get")
	@ResponseBody
	public String get(HttpEntity<String> httpEntity) {
		try {
			return getActivity(jsonUtils.getId(httpEntity));
		} catch (JsonParseException e) {
			return "Error Reading Content";
		}
	}

	public String getActivity(Long id) {
		return new API(stravaConnector.getToken()).getActivity(id, false).getName();
	}

	@GetMapping("/" + CONTROLLER_NAME + "/help")
	@ResponseBody
	public List<Help> help() {
		return getHelperMethods(this.getClass().getMethods());
	}

}
