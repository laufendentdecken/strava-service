package at.running.platform.strava.utils;

import java.lang.reflect.Method;
import java.util.Arrays;

import com.google.gson.Gson;

import javastrava.model.StravaActivity;

public class ParamExtractor {

	public static String extract(StravaActivity activity, String param) {
		Method method = Arrays.stream(activity.getClass().getMethods())
				.filter(m -> m.getName().equals("get" + param.substring(0, 1).toUpperCase() + param.substring(1)))
				.findFirst().orElse(null);
		if (method != null) {
			try {
				Object response = method.invoke(activity);
				if (response instanceof Long || response instanceof Integer || response instanceof Float) {
					return "\"" + param + "\":" + response.toString();
				}
				if (response instanceof String) {
					return "\"" + param + "\":\"" + response.toString() + "\"";
				}
				return new Gson().toJson(response);
			} catch (Exception e) {
			}
		}
		return null;
	}
}
