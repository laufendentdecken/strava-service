package at.running.platform.strava.utils;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

@Service
public class JsonUtils {
	public Long getId(HttpEntity<String> httpEntity) {
		JsonObject jsonObject = new Gson().fromJson(httpEntity.getBody(), JsonObject.class);
		JsonObject pipelineObject = getJsonObject(jsonObject, "pipeline");
		JsonObject contentObject = getJsonObject(pipelineObject, "content");
		JsonPrimitive idObject = getJsonPrimitive(contentObject, "id");
		return idObject.getAsLong();
	}

	public JsonPrimitive getJsonPrimitive(JsonObject jsonObject, String name) throws JsonParseException {
		JsonPrimitive object = jsonObject.getAsJsonPrimitive(name);
		if (object == null) {
			throw new JsonParseException("No Object[" + name + "] available");
		}
		return object;
	}
	
	public JsonObject getJsonObject(JsonObject jsonObject, String name) throws JsonParseException {
		JsonObject object = jsonObject.getAsJsonObject(name);
		if (object == null) {
			throw new JsonParseException("No Object[" + name + "] available");
		}
		return object;
	}
}