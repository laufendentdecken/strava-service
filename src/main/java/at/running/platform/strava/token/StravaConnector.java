package at.running.platform.strava.token;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javastrava.api.API;
import javastrava.api.AuthorisationAPI;
import javastrava.auth.model.Token;
import javastrava.auth.model.TokenResponse;

@Service
public class StravaConnector {
	
	@Value("${strava.client_id}")
	private Integer clientId;
	@Value("${strava.client_secret}")
	private String clientSecret;
	@Value("${strava.code}")
	private String code;
	
	public Token getToken() {
		AuthorisationAPI auth = API.authorisationInstance();
		TokenResponse response = auth.tokenExchange(clientId, clientSecret, code);
		Token token = new Token(response);
		return token;
	}

	public API getAPI() {
		return new API(getToken());
	}
}
