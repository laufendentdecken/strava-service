package at.running.platform.strava.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import at.running.platform.common.model.Help;
import at.running.platform.strava.token.StravaConnector;
import javastrava.api.API;
import javastrava.model.StravaActivity;
import javastrava.model.StravaLap;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StravaControllerTests {

	@Autowired
	private StravaController stravaController;

	@Test
	public void help() {
		List<Help> helpers = stravaController.help();
		assertTrue("No Helper Defintion found", helpers.size() > 0);
		assertTrue("Get Definition not found", helpers.contains(new Help("get", new ArrayList<>())));
	}

	@Test
	public void listOneElement() {
		API mockAPI = mock(API.class);
		StravaActivity[] activities = new StravaActivity[1];
		StravaActivity stravaActivity = new StravaActivity();
		stravaActivity.setId(123L);
		stravaActivity.setName("Test");
		activities[0] = stravaActivity;
		when(mockAPI.listAuthenticatedAthleteActivities(null, null, 1, 1)).thenReturn(activities);

		StravaConnector mockTocken = mock(StravaConnector.class);
		when(mockTocken.getAPI()).thenReturn(mockAPI);

		stravaController.setStravaConnector(mockTocken);
		String response = stravaController.list(1, null);

		assertEquals("[{\"id\":123,\"name\":\"Test\"}]", response);
	}

	@Test
	public void listOneElementWithOnlyId() {
		API mockAPI = mock(API.class);
		StravaActivity[] activities = new StravaActivity[1];
		StravaActivity stravaActivity = new StravaActivity();
		stravaActivity.setId(123L);
		activities[0] = stravaActivity;
		when(mockAPI.listAuthenticatedAthleteActivities(null, null, 1, 1)).thenReturn(activities);

		StravaConnector mockTocken = mock(StravaConnector.class);
		when(mockTocken.getAPI()).thenReturn(mockAPI);

		stravaController.setStravaConnector(mockTocken);
		String response = stravaController.list(1, "id");

		assertEquals("[{\"id\":123}]", response);
	}

	@Test
	public void listTwoElement() {
		API mockAPI = mock(API.class);
		StravaActivity[] activities = new StravaActivity[1];
		StravaActivity stravaActivity = new StravaActivity();
		stravaActivity.setId(123L);
		stravaActivity.setName("Name");
		activities[0] = stravaActivity;
		when(mockAPI.listAuthenticatedAthleteActivities(null, null, 1, 1)).thenReturn(activities);

		StravaConnector mockTocken = mock(StravaConnector.class);
		when(mockTocken.getAPI()).thenReturn(mockAPI);

		stravaController.setStravaConnector(mockTocken);
		String response = stravaController.list(1, "id,name");

		assertEquals("[{\"id\":123,\"name\":\"Name\"}]", response);
	}

	@Test
	public void getLaps() {
		API mockAPI = mock(API.class);
		StravaActivity[] activities = new StravaActivity[1];
		StravaActivity stravaActivity = new StravaActivity();

		List<StravaLap> laps = new ArrayList<>();
		StravaLap lap = new StravaLap();
		lap.setId(1L);
		laps.add(lap);
		stravaActivity.setLaps(laps);

		activities[0] = stravaActivity;
		when(mockAPI.listAuthenticatedAthleteActivities(null, null, 1, 1)).thenReturn(activities);

		StravaConnector mockTocken = mock(StravaConnector.class);
		when(mockTocken.getAPI()).thenReturn(mockAPI);

		stravaController.setStravaConnector(mockTocken);
		String response = stravaController.list(1, "laps");

		assertEquals("[{[{\"id\":1}]}]", response);
	}
}
